#!/bin/bash
#
# Update containerd v1.x configuration to use local image registry
#
# https://github.com/containerd/containerd/blob/main/docs/hosts.md
# https://cloudspinx.com/configuring-containerd-to-use-sonatype-nexus-registry-proxy/
#
# Usage, e.g.
# ===========
# for h in host1 host2 host3 ... ; do
#        scp airgap.sh $h:
#        ssh $h "sudo ./airgap.sh"   # "airgap.sh -r" to revert changes back
# done
# ============
#
# Two thing need to be updated below:
# REG is URL to your registry
# uncomment skip_verify if your registry works over HTTPS
#
# Art by doka@funlab.cc

BASE="/etc/containerd/config.toml"
if [ "$1" == "-r" ]; then
  mv $BASE.bak $BASE 2>/dev/null
  rm -rvf /etc/containerd/certs.d/_default
  systemctl restart containerd
  exit 0
fi

# The registry URL
REG="http://registry:5000"
cp $BASE $BASE.bak

sed -i '/\[plugins."io.containerd.grpc.v1.cri".registry\]$/a\ \ \ \ \ \ config_path = "\/etc\/containerd\/certs.d"\n' $BASE
sed -i '/config_path = "\/etc\/containerd\/certs.d"/{n;N;d}' $BASE
diff -u $BASE.bak $BASE
echo
echo -n "Is everything correct (y/n)? "
read qqq

if [[ "$qqq" =~ ^Y|y ]]; then
  mkdir -p /etc/containerd/certs.d/_default
#--
  cat <<EOF > /etc/containerd/certs.d/_default/hosts.toml
[host."$REG"]
  capabilities = ["pull", "resolve", "push"]
  #skip_verify = true
EOF
  systemctl restart containerd
#--
else
  mv $BASE.bak $BASE
fi

