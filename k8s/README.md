## K8s quickstart

### Prerequisites
All Kubernetes nodes must resolve each other, either through DNS or /etc/hosts

### Procedure
Set of scripts to install Kubernetes cluster (flannel, metallb, ingress-nginx)
- Copy this directory to your primary controller (which will be bootstrapped)
- Ensure your user id is capable to SSH to other hosts of planned cluster and has passwordless sudo
- Check / update k8init.cfg per your installation
- Install Kubernetes on the node: `sudo 00-k8install.sh`
- Bootstrap the node: `sudo 10-k8init.sh`. The script uses **k8init.cfg** to bootstrap the cluster and generate configurations for other nodes, which (configurations) need to be applied on every respective node.
  - **NOTE:**
    - if you're going to use more than one controller, load balancer need to be configured before running this script (see addons/haproxy_add.cfg)
    - script does not do sanity / validation / consistency checks of parameters in k8init.cfg. This is your responsiblity.
    - before running it, you can do '10-k8init.sh --dry-run' to make _some_ checks
  - The script will produce the output like this:
```
[WARNING] Validation of /home/trinity/k8s/pki/ca.crt and/or /home/trinity/k8s/pki/ca.key failed, skipping copying.
[INFO] Ignore this warning if you weren't preparing these files
[INFO] OR you have 5 seconds to press Ctrl-C to stop installation
[init] Using Kubernetes version: v1.31.0
[ ... ]
**** Environment prepared, you can now run kubectl as root on this host
**** For other users, run the following command as the user:
      mkdir -p $HOME/.kube
      sudo cp -i /etc/kubernetes/admin.conf $HOME/.kube/config
      sudo chown $(id -u):$(id -g) $HOME/.kube/config

**** kc1 bootstrapped. To join other hosts to the cluster, run:

     scp 00-k8install.sh kc2: ; ssh kc2 'sudo ./00-k8install.sh'"
     scp gen-join-kc2.yml kc2: ; ssh kc2 'sudo kubeadm join --config gen-join-kc2.yml'

     scp 00-k8install.sh kc3: ; ssh kc3 'sudo ./00-k8install.sh'"
     scp gen-join-kc3.yml kc3: ; ssh kc3 'sudo kubeadm join --config gen-join-kc3.yml'

     scp 00-k8install.sh kw1: ; ssh kw1 'sudo ./00-k8install.sh'"
     scp gen-join-kw1.yml kw1: ; ssh kw1 'sudo kubeadm join --config gen-join-kw1.yml'

     scp 00-k8install.sh kw2: ; ssh kw2 'sudo ./00-k8install.sh'"
     scp gen-join-kw2.yml kw2: ; ssh kw2 'sudo kubeadm join --config gen-join-kw2.yml'

**** When configured hosts will join the cluster and
****   will have the 'Ready' status (kubectl get node), you can continue with next step (15-k8svc.sh)
```
- copy .kube/config to be able to work with Kubernets from your user id
  - check whether `kubectl get node` works and returns your first node Ready:
      ```
      user@kc1:~/k8s$ kubectl get node
      NAME   STATUS   ROLES           AGE   VERSION
      kc1    Ready    control-plane   45s   v1.31.0
      ```
- run commands (scp/ssh) from the script's output - this will join your controllers and workers to the cluster
- check whether Kubernetes is operable:
  - run Alpine container: `kubectl run my-shell --rm -i --tty --image alpine -- sh`
  - check connectivity and DNS resolution:
    - `traceroute google.com` if IPv4 configured
    - `traceroute6 google.com` if IPv6 configured
    - or anything else accessible if network isolated from the Internet
- run 15-k8svc.sh after other nodes will join cluster and will become 'Ready' (kubectl get node) - it will install MetalLB load balancer and, if specified in k8init.cfg, NGINX Ingress Controller.

### Additional configuration
- addons/MetalLB-conf-BGP.yaml need to be updated according to _your_ environment and then applied using `kubectl apply -f addons/MetalLB-conf-BGP.yml`. This configuration, as the name suggests, is for BGP peering - you need either FRR or Bird to communicate with MetalLB. Otherwise, use [MetalLB configuration guide](https://metallb.universe.tf/configuration/) to configure it for L2/ARP mode.

## Join additional nodes
To join additional nodes, just run `sudo joinNode.sh ctrl|worker node_name`, e.g.
```
$ sudo ./joinNode.sh worker kw3
**** To join host kw3 to the cluster, run:
     scp ./00-k8install.sh kw3: ; ssh kw3 'sudo ./00-k8install.sh'
     scp gen-join-kw3.yml kw3: ; ssh kw3 'sudo kubeadm join --config gen-join-kw3.yml'
```
The script also uses configuration from k8init.cfg to populate the new node.

## End notice

**LEGAL DISCLAIMER:** use these scripts on your own risk.

Art by doka@funlab.cc
