#!/bin/bash
#
# Art by doka@funlab.cc
#
# https://kubernetes.io/docs/setup/production-environment/tools/kubeadm/dual-stack-support/
# https://kubernetes.io/docs/reference/config-api/kubeadm-config.v1beta4/
# https://kubernetes.io/docs/concepts/services-networking/dual-stack/
# https://kubernetes.io/docs/reference/setup-tools/kubeadm/kubeadm-init/
# https://kubernetes.io/docs/tasks/administer-cluster/kubeadm/kubeadm-certs/
# https://github.com/kubernetes/kubernetes/blob/master/pkg/proxy/ipvs/README.md

if [ $(id -u) -gt 0 ]; then
  echo "Need to be root to run this command, exiting ..."
  exit 1
fi

[ "$1" == "--dry-run" ] && DRY_RUN=true || DRY_RUN=false

set -e
APP_DIR=$(dirname $(realpath $0))
source $APP_DIR/k8init.cfg
source $APP_DIR/include/functions.sh
chmod a+x $APP_DIR/include/lh_vol_update.sh
set +e

# All required for this step variables are
# already assigned in include/functions.sh
untemplate $APP_DIR/templates/init-template.yml gen-init.yml

CRT="$APP_DIR/pki/ca.crt"
KEY="$APP_DIR/pki/ca.key"
[ -f "$CRT" ] && openssl x509 -in $CRT >/dev/null 2>&1 && CRT_OK=true || CRT_OK=false
[ -f "$KEY" ] && openssl rsa -in $KEY -check -passin pass:1234 >/dev/null 2>&1 && KEY_OK=true || KEY_OK=false
if $CRT_OK && $KEY_OK ; then
  if $DRY_RUN; then echo $CRT / $KEY are ok
  else
    # Path to certificates either custom or default
    ca_path=$(cat $APP_DIR/templates/init-template.yml | awk '$1 ~ /^certificatesDir\s*:/ { print $NF }')
    [-z "$ca_path" ] &&
      ca_path=$(kubeadm config print init-defaults | awk '$1 ~ /^certificatesDir\s*:/ { print $NF }')
    mkdir -p $ca_path/etcd
    # K8s CA
    install -v -o root -g root -m 644 $CRT $ca_path
    install -v -o root -g root -m 600 $KEY $ca_path
    # ETCD CA
    install -v -o root -g root -m 644 $CRT $ca_path/etcd
    install -v -o root -g root -m 600 $KEY $ca_path/etcd
  fi
else
  echo "[WARNING] Validation of $CRT and/or $KEY failed, skipping copying."
  echo "[INFO] Ignore this warning if you weren't preparing these files"
  echo "[INFO] OTHERWISE you have 5 seconds to press Ctrl-C to stop installation"
  $DRY_RUN || sleep 6
fi

LOG="/tmp/kubeadm-init-$(date +%Y%m%d-%H%M).log"
touch $LOG
$DRY_RUN || kubeadm init --config=gen-init.yml --upload-certs 2>&1 | tee $LOG

echo
echo
echo "**************************************************************************************"
echo
echo "The full log above is available in $LOG"
echo

KUBECTL=$(cat $LOG | egrep "mkdir|sudo")
rm -fv $HOME/.kube/config
eval "$(echo "$KUBECTL" | sed -- 's/cp -i/cp -fv/g')"

if [ -n "$CNI" ] && [ -f "$APP_DIR/include/${CNI}_install.sh" ]; then
  source $APP_DIR/include/${CNI}_install.sh
else
  echo
  echo ">>>> CNI Plugin is not specified or installation module is not available"
  echo
fi

echo
echo "**** Environment prepared, you can now run kubectl as $(whoami) on this host"
echo "**** For other users, run the following command as the user:"
echo "$KUBECTL" | sed 's/^/    /g'
echo

### Prepare configuration for joining other nodes

CA_CERT_HASH=$(cat $LOG | egrep -o -- "--discovery-token-ca-cert-hash sha256:[a-f0-9]+" | awk '{print $2}' | head -1)
CERT_KEY=$(cat $LOG | egrep -o -- "--certificate-key [a-f0-9]+" | awk '{print $2}' | head -1)

echo "**** $(hostname) bootstrapped. To join other hosts to the cluster, run:"

# Prepare join files for OTHER CONTROLLER nodes
for host in $CTRLS; do
  if [ "$host" == "$(hostname)" ] || [ "$host" == "$(hostname --fqdn)" ]; then
    # Do not generate init/join for the master host
    continue
  fi
  yml_name="gen-join-$host.yml"
  # Don't care about CP_ENDPOINT - it was already defined earlier
  determine_IPs $host
  untemplate $APP_DIR/templates/join-ctrl-template.yml $yml_name
  echo
  echo "     scp $APP_DIR/00-k8install.sh $host: ; ssh $host 'sudo ./00-k8install.sh'"
  echo "     scp $yml_name $host: ; ssh $host 'sudo kubeadm join --config $(basename $yml_name)'"
done 

# Prepare join files for WORKER nodes
for host in $WORKERS; do
  yml_name="gen-join-$host.yml"
  # Don't care about CP_ENDPOINT - it was already defined earlier
  determine_IPs $host
  untemplate $APP_DIR/templates/join-worker-template.yml $yml_name
  echo
  echo "     scp $APP_DIR/00-k8install.sh $host: ; ssh $host 'sudo ./00-k8install.sh'"
  if $LHIO ; then
    if $LHIO_VOL ; then
      echo "     scp $APP_DIR/include/lh_vol_update.sh $host: ; ssh $host 'sudo ./lh_vol_update.sh'"
    else
      echo "     scp $APP_DIR/include/lh_vol_update.sh $host: ; ssh $host 'sudo ./lh_vol_update.sh --skip-volume'"
    fi
  fi
  echo "     scp $yml_name $host: ; ssh $host 'sudo kubeadm join --config $(basename $yml_name)'"
done 

echo
echo "**** When configured hosts will join the cluster and"
echo "**** will have the 'Ready' status (kubectl get node), you can continue with next step (15-k8svc.sh)"
echo
