#!/bin/bash
#
# Art by doka@funlab.cc

APP_DIR=$(dirname $(realpath $0))
source $APP_DIR/k8init.cfg

if $METALLB ; then
  helm repo add metallb https://metallb.github.io/metallb
  helm repo update
  if [ -z "$METALLB_BGP" ]; then
    [ $CNI == "calico" ] && METALLB_BGP=false || METALLB_BGP=true
  fi
  # BGP/IPv6 support available only in FRR mode
  $METALLB_BGP && SPEAKER_SET="speaker.frr.enabled=true" || SPEAKER_SET="speaker.enabled=false"
  helm install metallb metallb/metallb --set $SPEAKER_SET --namespace metallb-system --create-namespace

  echo
  echo "**** Kubernetes Load Balancer (MetalLB) installed."
  echo "**** Edit $APP_DIR/addons/MetalLB-conf.yml and run 'kubectl apply -f $APP_DIR/addons/MetalLB-conf.yml'"
  echo
fi

if $NGINX_INGRESS ; then
  helm upgrade --install ingress-nginx ingress-nginx \
    --repo https://kubernetes.github.io/ingress-nginx \
    --namespace ingress-nginx --create-namespace

  echo
  echo "**** NGINX Ingress installed."
  echo "**** Run 'kubectl edit service/ingress-nginx-controller -n ingress-nginx'"
  echo "****    and configure IPv4/v6 dual-stack and fixed addresses for the Ingress"
  echo
fi

if $LHIO ; then
  # update default values before helming it
  helm repo add longhorn https://charts.longhorn.io
  helm repo update
  longhorn_v=$(curl -s https://api.github.com/repos/longhorn/longhorn/releases/latest | jq -r .tag_name)
  if $LHIO_VOL ; then
    defaultDataPath="--set defaultSettings.defaultDataPath=/lhio"
    defaultReservedPct="--set defaultSettings.storageReservedPercentageForDefaultDisk=0"
  fi
  helm install longhorn longhorn/longhorn --namespace longhorn-system --create-namespace --version ${longhorn_v:1} \
    --set defaultSettings.storageMinimalAvailablePercentage=0 \
    --set defaultSettings.defaultReplicaCount=2 \
    $defaultDataPath $defaultReservedPct

  echo
  echo "**** Longhorn installed."
  echo "**** To configure access to the Longhorn UI, follow the instruction:"
  echo "****    https://longhorn.io/docs/${longhorn_v:1}/deploy/accessing-the-ui/longhorn-ingress/"
  echo
fi

echo

