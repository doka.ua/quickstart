#!/bin/bash

function stop() {
    msg=${1:-"******* ERROR CREATING STORAGE FOR LONHGHORN"}
    echo
    echo "$msg, check what is wrong with this"
    echo
    exit 1
}

echo "dm_crypt" > /etc/modules-load.d/lhio
modprobe dm_crypt
apt install -y open-iscsi nfs-common

if [ "$1" == "--skip-volume" ]; then
    echo "Skipping Volume creation as requested"
    exit 0
fi

LHIO_PRESENT=$(vgs | awk '$1 == "lhio" { print "true" } END { print "false" }' | head -1)
for unknown_vol in $(parted -lm 2>/dev/null | grep "Block" | awk -F: '$6 == "unknown" {printf("%s\n", $1)}'); do
    echo "--> Checking $unknown_vol"
    # Check if volume partitioned
    disk_id=$(basename $unknown_vol)
    fdisk -l $unknown_vol | egrep -q "$disk_id[0-9]+" && continue
    # If nevertheless we're unable to add it to the LVM
    pvcreate $unknown_vol 2>/dev/null || continue
    $LHIO_PRESENT && vgextend lhio $unknown_vol || vgcreate lhio $unknown_vol
    [ $? -eq 0 ] || stop
    $LHIO_PRESENT && lvextend -l +100%FREE /dev/lhio/vol || lvcreate -l100%FREE -n vol lhio
    [ $? -eq 0 ] || stop
    $LHIO_PRESENT && resize2fs /dev/mapper/lhio-vol || mkfs.ext4 /dev/mapper/lhio-vol
    [ $? -eq 0 ] || stop
done

LHIO_PRESENT=$(vgs | awk '$1 == "lhio" { print "true" } END { print "false" }' | head -1)
# If no volume created - exit with error
$LHIO_PRESENT || stop "*** NO VOLUME FOR LONGHORN FOUND"

mkdir -p /lhio
sed -iE '/^\/dev\/mapper\/lhio-vol/d' /etc/fstab
echo "/dev/mapper/lhio-vol	/lhio   ext4   defaults,discard  0  1" >> /etc/fstab
systemctl daemon-reload
mount -a

exit 0
