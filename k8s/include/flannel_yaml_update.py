#!/usr/bin/env python3
#
# Art by doka@funlab.cc

import sys
import os
import yaml
import json

flannel = {
    "EnableIPv4": False,
    "EnableIPv6": False,
    "EnableNFTables": False,
    "Backend": {}
}

BACKENDS=["wireguard", "vxlan", "host-gw"]
BE_DEFAULT="wireguard"

if pod4 := os.environ.get("POD_SUBNET4", None) :
    flannel["EnableIPv4"] = True
    flannel["Network"] = pod4

if pod6 := os.environ.get("POD_SUBNET6", None) :
    flannel["EnableIPv6"] = True
    flannel["IPv6Network"] = pod6

backend = os.environ.get("CNI_BACKEND", "wireguard").lower()
if backend not in BACKENDS:
    backend = BE_DEFAULT

flannel["Backend"]["Type"] = backend
if backend == "wireguard" :
    flannel["Backend"]["PSK"] = os.environ["WG_PSK"]
    flannel["Backend"]["Mode"] = "separate"

with open(sys.argv[1]) as f:
    y = yaml.safe_load(f)
    y['data']['net-conf.json'] = json.dumps(flannel, ensure_ascii=True, indent=2)

with open(sys.argv[1], "w") as f:
    yaml.dump(y, f, default_flow_style=False, sort_keys=False)

