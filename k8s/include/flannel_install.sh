#
# This code running from 10-k8init.sh as 'source flannel_install.sh'

output="kube-flannel.yml"
curl -sL https://github.com/flannel-io/flannel/releases/latest/download/kube-flannel.yml -o $output

# Update flannel init configuration
# -- separate flannel template into different YAML files
prefix=kuflan
csplit --digits=2  --quiet --prefix=$prefix $output "/^---$/" "{*}"
for f in ${prefix}*; do
  sed -i '/^---/d' $f
  # Find fragment with network definition
  grep -q "net-conf.json" $f && soi=$f
done
# Update the net-conf.json
export WG_PSK=$(wg genpsk)
export POD_SUBNET4
export POD_SUBNET6
export CNI_BACKEND
$APP_DIR/include/flannel_yaml_update.py $soi
# Rewrite original template
> $output
for f in ${prefix}*; do
  cat $f >> $output
  echo >> $output
  echo "---" >> $output
  rm -f $f
done
# Remove the last "---" separator
sed -i '$ d' $output
$DRY_RUN || kubectl apply -f $output

echo
echo "**** Kubernetes networking (Flannel) installed, init config is in $(pwd)/$output"
echo
