#
# Art by doka@funlab.cc
#
# This file is included by another scripts and not intended to run autonomously
#

untemplate() {
  cat $1 | sed "
    s|%POD_SUBNETS%|${POD_SUBNETS}|g;
    s|%SVC_SUBNETS%|${SVC_SUBNETS}|g;
    s|%CP_ENDPOINT%|${CP_ENDPOINT}|g;
    s|%LOCAL_EP%|${LOCAL_EP}|g;
    s|%LOCAL_IPS%|${SVC_IPS}|g;
    s|%BIND_PORT%|${BIND_PORT}|g;
    s|%BOOTSTRAP_TOKEN%|${BOOTSTRAP_TOKEN}|g;
    s|%CA_CERT_HASH%|${CA_CERT_HASH}|g;
    s|%CERT_KEY%|${CERT_KEY}|g;
    s|%KUBE_PROXY_MODE%|${KUBE_PROXY_MODE}|g;
    " > $2
}
  
csv() {
  s="" ; for entry in $*; do s+=",$entry"; done
  # Remove leading and trailing commas
  echo $s | sed -E 's/^,+|,+$//g'
}

determine_IPs () {
  SVC_IP4=$(host $1 $DNS_SRV | grep "has address" | head -1 | awk '{print $NF}')
  SVC_IP6=$(host $1 $DNS_SRV | grep "has IPv6 address" | head -1 | awk '{print $NF}')
  if [[ "$SVC_IP4" =~ ^127 ]] || [ "$SVC_IP6" == "::1" ]; then
    echo "Either IPv4 or IPv6 address of $1 points to localhost, stopping ..."
    exit 1
  fi
  # If both addresses are available but IPv6 is preferable
  if [ -n "$SVC_IP6" ] && [ -n "$SVC_IP4" ] && $LOCAL_API_ENDPOINT_IPV6 ; then
    LOCAL_EP=$SVC_IP6
    [ -z "$CP_ENDPOINT" ] && CP_ENDPOINT="[$SVC_IP6]:6443"
  # else choose IPv4 if available
  elif [ -n "$SVC_IP4" ]; then
    LOCAL_EP=$SVC_IP4
    [ -z "$CP_ENDPOINT" ] && CP_ENDPOINT="$SVC_IP4:6443"
  # else choose IPv6 if available
  elif [ -n "$SVC_IP6" ]; then
    LOCAL_EP=$SVC_IP6
    [ -z "$CP_ENDPOINT" ] && CP_ENDPOINT="[$SVC_IP6]:6443"
  else
    echo "$1 cannot be resolved in DNS, exiting ..."
    exit 1
  fi
  SVC_IPS=$(csv $SVC_IP4 $SVC_IP6)
}

[ -z "$LOCAL_API_ENDPOINT_IPV6" ] && LOCAL_API_ENDPOINT_IPV6=false
export POD_SUBNETS=$(csv $POD_SUBNET4 $POD_SUBNET6)
export SVC_SUBNETS=$(csv $SVC_SUBNET4 $SVC_SUBNET6)

if [ -z "$POD_SUBNETS" ] || [ -z "$SVC_SUBNETS" ]; then
  echo "Either POD_SUBNETS or SVC_SUBNETS undefined. Is k8init.cfg loaded?"
  exit 1
fi

determine_IPs $(hostname)
BIND_PORT=$(echo $CP_ENDPOINT | awk -F: '{print $NF}')
BOOTSTRAP_TOKEN="$(tr -dc 0-9a-z </dev/urandom | head -c 6).$(tr -dc 0-9a-z </dev/urandom | head -c 16)"

[ -z "$CNI_BACKEND" ] && CNI_BACKEND="wireguard"

# Not implmented, ignoring the config
# Both IPVS and nftables give performance gain starting thousands of services
# Looks not actual at the moment
KUBE_PROXY_MODE=""

#case $KUBE_PROXY_MODE in
#  iptables)
#    ;;
#  nftables)
#    ;;
#  ipvs)
#    ;;
#  *)
#    ;;
#esac
