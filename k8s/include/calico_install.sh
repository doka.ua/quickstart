#
# This code running from 10-k8init.sh as 'source calico_install.sh'

CALICO_CR="calico_custom_resources.yaml"

calico_v=$(curl -s https://api.github.com/repos/projectcalico/calico/releases/latest | jq -r .tag_name)
kubectl create -f https://raw.githubusercontent.com/projectcalico/calico/${calico_v}/manifests/tigera-operator.yaml

failed=true
n=0
while [ $n -lt 24 ]; do
  # Wait until Operator is functional and do everything else
  if [ -n "$(kubectl get pod -n tigera-operator 2>/dev/null | grep Running)" ]; then
    cat $APP_DIR/include/$CALICO_CR | sed "
        s|%POD_SUBNET4%|${POD_SUBNET4}|g;
        s|%POD_SUBNET6%|${POD_SUBNET6}|g;
    " > /tmp/$CALICO_CR

    [ -z "$POD_SUBNET4" ] && sed -i '/name: default-ipv4-ippool/{N;N;N;N;N;d}' /tmp/$CALICO_CR
    [ -z "$POD_SUBNET6" ] && sed -i '/name: default-ipv6-ippool/{N;N;N;N;N;d}' /tmp/$CALICO_CR

    kubectl apply -f /tmp/$CALICO_CR
    
    # White until Calico started to report about this
    m=0
    while [ $m -lt 24 ]; do
      if [ -n "$(kubectl api-resources | grep FelixConfiguration | egrep '\sprojectcalico')" ]; then
        echo
        echo "**** Kubernetes networking (Calico) installed"
        echo "**** Additional configuration for Calico can be found in $APP_DIR/addons/Calico-conf-BGP.yaml"
        echo
        failed=false
        break
      fi

      echo "Waiting for Calico is running ..."
      ((m++))
      sleep 5
    done

    if ! $failed ; then
      # If it is not failed, do ALL OTHER updates
  
      # https://docs.tigera.io/calico/latest/network-policy/encrypt-cluster-pod-traffic
      if [ "$CNI_BACKEND" == "wireguard" ]; then
        [ -n "$POD_SUBNET4" ] && kubectl patch felixconfiguration default --type='merge' -p '{"spec":{"wireguardEnabled":true}}'
        [ -n "$POD_SUBNET6" ] && kubectl patch felixconfiguration default --type='merge' -p '{"spec":{"wireguardEnabledV6":true}}'
        echo
        echo ">>> Calico/Wireguard enabled"
      fi
      # ... anything else comes here ...
    fi
    # We're done, stop here
    break
  fi

  echo "Waiting for Calico operator is running ..."
  ((n++))
  sleep 5
done

if $failed ; then
  echo
  echo "**** Kubernetes networking (Calico) installation failed. Custom configuration stored in /tmp/$CALICO_CR"
fi

echo
