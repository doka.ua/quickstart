#!/bin/sh
#
# Art by doka@funlab.cc

if [ $(id -u) -gt 0 ]; then
  echo "Need to be root to run this command, exiting ..."
  exit 1
fi

swapoff -a
sed -E -i '/\s+swap\s+/d' /etc/fstab

tee /etc/modules-load.d/containerd.conf <<EOF
overlay
br_netfilter
EOF
modprobe overlay
modprobe br_netfilter

# Prepare for IPVS mode, even if not configured (to be able to switch later)
tee /etc/modules-load.d/ipvs.conf <<EOF
ip_vs
ip_vs_rr
ip_vs_wrr
ip_vs_sh
nf_conntrack
EOF
modprobe ip_vs
modprobe ip_vs_rr
modprobe ip_vs_wrr
modprobe ip_vs_sh
modprobe nf_conntrack

# 99-z is to have it last
tee /etc/sysctl.d/99z_k8s.conf <<EOF
net.bridge.bridge-nf-call-ip6tables = 1
net.bridge.bridge-nf-call-iptables = 1
net.ipv4.ip_forward = 1
net.ipv6.conf.all.forwarding = 1
EOF
sysctl --system

export DEBIAN_FRONTEND=noninteractive
mkdir -p -m 755 /etc/apt/keyrings

curl -fsSL https://baltocdn.com/helm/signing.asc | gpg --yes --dearmor -o /etc/apt/keyrings/helm.gpg
chmod 644 /etc/apt/keyrings/helm.gpg
echo "deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/helm.gpg] https://baltocdn.com/helm/stable/debian/ all main" | tee /etc/apt/sources.list.d/helm-stable-debian.list
chmod 644 /etc/apt/sources.list.d/helm-stable-debian.list

curl -fsSL https://pkgs.k8s.io/core:/stable:/v1.32/deb/Release.key | gpg --yes --dearmor -o /etc/apt/keyrings/k8sapt-keyring.gpg
chmod 644 /etc/apt/keyrings/k8sapt-keyring.gpg
echo 'deb [signed-by=/etc/apt/keyrings/k8sapt-keyring.gpg] https://pkgs.k8s.io/core:/stable:/v1.32/deb/ /' | tee /etc/apt/sources.list.d/kubernetes.list
chmod 644 /etc/apt/sources.list.d/kubernetes.list

curl -fsSL https://download.docker.com/linux/ubuntu/gpg | gpg --yes --dearmour -o /etc/apt/trusted.gpg.d/docker.gpg
chmod 644 /etc/apt/trusted.gpg.d/docker.gpg
add-apt-repository -y "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"

apt-get -y update
apt-get install -y \
    curl jq gnupg2 software-properties-common apt-transport-https ca-certificates \
    wireguard wireguard-tools \
    ipvsadm ipset \
    containerd.io helm kubelet kubeadm kubectl
apt-mark hold kubelet kubeadm kubectl containerd.io

containerd config default | tee /etc/containerd/config.toml >/dev/null 2>&1
sed -i 's/SystemdCgroup \= false/SystemdCgroup \= true/g' /etc/containerd/config.toml

# https://github.com/containerd/containerd/issues/8139
#  \--> https://github.com/kubernetes-sigs/cri-tools/issues/1089
cat <<EOF > /etc/crictl.yaml
#runtime-endpoint: "unix:///run/containerd/containerd.sock"
timeout: 0
debug: false
EOF
systemctl restart containerd
systemctl enable containerd

