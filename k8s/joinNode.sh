#!/bin/bash
#
# Art by doka@funlab.cc

if [ $(id -u) -gt 0 ]; then
  echo "Need to be root to run this command, exiting ..."
  exit 1
fi

set -e
APP_DIR=$(dirname $(realpath $0))
source $APP_DIR/k8init.cfg
source $APP_DIR/include/functions.sh
set +e

DRY_RUN=false
while [ -n "$1" ]; do
  case $1 in
    worker|ctrl)
      KIND="$1"
      shift;
      NODE_NAME=$1
      ;;
    *)
      echo "Unknown arg '$1', ignoring"
      ;;
  esac
  shift
done

if [ -z "$KIND" ] || [ -z "$NODE_NAME" ]; then
  echo "Usage: $0 worker|ctrl node_name"
  exit 1
fi

CERT_KEY=$(kubeadm init phase upload-certs --upload-certs | sed -n '/certificate key/ {n;p}')
JOIN_CMD=$(kubeadm token create --print-join-command)
BOOTSTRAP_TOKEN=$(echo "$JOIN_CMD" | egrep -o -- "--token [a-z0-9]{6}\.[a-z0-9]{16}" | awk '{print $2}' | head -1)
CA_CERT_HASH=$(echo "$JOIN_CMD" | egrep -o -- "--discovery-token-ca-cert-hash sha256:[a-f0-9]+" | awk '{print $2}' | head -1)

### Prepare configuration for joining node

[ "$KIND" == "ctrl" ] && TMPL="$APP_DIR/templtes/join-ctrl-template.yml" || TMPL="$APP_DIR/templates/join-worker-template.yml"
yml_name="gen-join-${NODE_NAME}.yml"

determine_IPs ${NODE_NAME}
# Don't care about CP_ENDPOINT - it was already defined earlier
untemplate $TMPL $yml_name

echo "**** To join host ${NODE_NAME} to the cluster, run:"
echo "     scp $APP_DIR/00-k8install.sh ${NODE_NAME}: ; ssh ${NODE_NAME} 'sudo ./00-k8install.sh'"
if $LHIO ; then
  if $LHIO_VOL ; then
    echo "     scp $APP_DIR/include/lh_vol_update.sh ${NODE_NAME}: ; ssh ${NODE_NAME} 'sudo ./lh_vol_update.sh'"
  else
    echo "     scp $APP_DIR/include/lh_vol_update.sh ${NODE_NAME}: ; ssh ${NODE_NAME} 'sudo ./lh_vol_update.sh --skip-volume'"
  fi
fi
echo "     scp $yml_name ${NODE_NAME}: ; ssh ${NODE_NAME} 'sudo kubeadm join --config $(basename $yml_name)'"
