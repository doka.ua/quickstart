## Various quickstart scripts

These scripts intended to run on Ubuntu-based systems

1. proxtibidox/ is very simple CLI-based VMs-on-baremetal management tool
2. k8s/ contain set of scripts to install Kubernetes cluster (flannel, metallb, ingress-nginx)

Other:

3. aerospike.yml is a Manifest to run Aerospike cluster in the Kubernetes. Created to overcome Aerospike's community edition limitation - absense of encryption. So if you configured K8s to encrypt inter-node traffic, then Aerospike traffic will be encrypted by underlying networking.

**LEGAL DISCLAIMER:** use these scripts on your own risk.

Art by doka@funlab.cc
