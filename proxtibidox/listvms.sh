#!/bin/bash
#
# Art by doka@funlab.cc

# Where VM disks located
IMG="/var/lib/libvirt/images"
# Where DHCP leases stored
LEASES="/etc/dnsmasq.d/var/dnsmasq.leases"

for vm in $(virsh list --all | awk '$1 ~ /[0-9]|^-$/ { print $2 }'); do
  VMs="$VMs $vm"
done

# State:          running
# CPU(s):         1
# Max memory:     2097152 KiB
# Autostart:      disable

# cat /etc/dnsmasq.d/var/dnsmasq.leases
# 1723554750 52:54:00:ce:11:fc 100.73.0.240 kc1 ff:56:50:4d:98:00:02:00:00:ab:11:06:54:29:db:26:28:8d:0d
# 1723614305 1448103320 2a01:4f8:140:439a:1715::253 kc1 00:02:00:00:ab:11:06:54:29:db:26:28:8d:0d

echo "VM Name     State       CPU  RAM  Disk Autostart    MAC Address        IPv4 Address     IPv6 Address                             Data Disk(s)"
echo "----------  ----------  ---  ---  ---- -----------  -----------------  ---------------  ---------------------------------------  ------------"
for vm in $VMs; do
  info=$(virsh dominfo $vm)
  STATE=$(echo "$info" | awk '$1 ~ /^State/ { print $2 }')
  CPU=$(echo "$info" | awk '$1 ~ /^CPU\(/ { print $2 }')
  MEM=$(echo "$info" | awk '$0 ~ /^Max memory/ { print $3 }')
  AUTO=$(echo "$info" | awk '$1 ~ /^Autostart/ { print $2 }')
  DISK=$(stat -c "%s" $IMG/$vm/$vm.img); DISK=$((DISK/1024/1024/1024))
  IP4="" ; IP6="" ; MAC=""
  UPTIME=""
  # Whether we're using DNSMasq as DNS server?
  [ -n "$(netstat -lpu | grep -w domain | grep dnsmasq)" ] && DNS_LOCAL=true || DNS_LOCAL=false
  if $DNS_LOCAL && [ -r "$LEASES" ] ; then
    DNSM="$(cat $LEASES | grep -w $vm)"
    read IP4 MAC <<< "$(echo "$DNSM" | awk '$3 ~ /\./ {print $3 " " $2}' | head -1)"
    read IP6 <<< "$(echo "$DNSM" | awk '$3 ~ /:/ {print $3}' | head -1)"
  else
    IP4="unmanaged"
    IP6="unmanaged"
  fi
  [ -z "$MAC" ] && MAC=$(virsh dumpxml $vm | grep -w mac | sed -En "s/.*'([0-9a-z:]+)'.*/\1/p")
  if [ "$STATE" = "running" ]; then
    PID=$(virsh -c qemu:///system qemu-agent-command $vm \
            "{\"execute\": \"guest-exec\", \"arguments\": { \"path\": \"uptime\", \"capture-output\": true }}" \
            --pretty 2>/dev/null | jq -r '.return.pid')
    UPTIME="$(virsh -c qemu:///system qemu-agent-command $vm \
            "{\"execute\": \"guest-exec-status\", \"arguments\": { \"pid\": $PID }}" \
            --pretty 2>/dev/null | jq -r '.return."out-data"' | base64 -d)"
  fi
  DADS=""
  for dad in $(ls $IMG/$vm/$vm*tached.raw 2>/dev/null) ; do
    read DAD_ID DAD_STATUS <<< "$(basename $dad | awk -F. '{print $(NF-1)}' | awk -F- '{printf("%s %s\n", $(NF-2), $NF)}')"
    DAD_SIZE=$(stat -c "%s" $dad); DAD_SIZE=$((DAD_SIZE/1024/1024/1024))
    DAD_ID="$DAD_ID/${DAD_SIZE}G"
    [ "$DAD_STATUS" == "detached" ] && DAD_ID="[$DAD_ID]"
    DADS+=", $DAD_ID"
  done
  printf "%-10s  %-10s  %-3d  %-3s  %-4s %-11s  %-17s  %-15s  %-39s  %s\n" \
    $vm $STATE $CPU "$((MEM/1024/1024))G" $DISK $AUTO ${MAC:-"-"} ${IP4:-"-"} ${IP6:-"-"} "${DADS:2}"
  [ -n "$UPTIME" ] && printf "  \-> %s\n" "$UPTIME"
done

