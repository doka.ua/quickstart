#!/bin/bash
#
# Art by doka@funlab.cc

IMG="/var/lib/libvirt/images"
# Where DHCP leases stored
LEASES="/etc/dnsmasq.d/var/dnsmasq.leases"

VM_NAME=$1
if [ -z "$VM_NAME" ] ; then
  echo "Usage: $0 vm_name [--unattended]"
  echo
  exit 1
fi
shift

UNATT=false
while [ -n "$1" ]; do
  case $1 in
    -u|--unattended)
      UNATT=true
      ;;
  esac
  shift
done

if ! $UNATT; then
  read -p "Do you really want to remove $VM_NAME ? " yn
  [[ $yn =~ ^Y|y ]] || exit 0
fi

virsh destroy $VM_NAME
virsh undefine $VM_NAME
rm -rf $IMG/$VM_NAME
[ -r $LEASES ] && sed -i "/\s$VM_NAME\s/d" $LEASES
systemctl is-active dnsmasq 2>/dev/null && systemctl restart dnsmasq
ssh-keygen -f ~/.ssh/known_hosts -R $VM_NAME

exit 0
