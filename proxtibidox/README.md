# Proxtibidox is oversimplified VM management

If you have baremetal box with preinstalled Ubuntu - you're almost done.
Scripts / templates are for Ubuntu, but can be easily ported to other package managers.

## Pre-requisite
```
apt -y install qemu-kvm libvirt-daemon libvirt-daemon-system genisoimage virtinst whois
apt -y install openvswitch-switch
apt -y install dnsmasq

ovs-vsctl add-br br-ex
```
Note: package `whois` provides `mkpasswd` command

Configure DNSMasq using etc-dnsmasq/ as a reference. To run DNSMasq smoothly, stop/disable systemd-resolved.

By default, libvirt creates 'default' network with respective interface 'virbr0', you don't need it and it can be destroyed:
```
virsh net-destroy default
virsh net-undefine default
```

## How it looks
```
# listvms.sh 
VM Name     State       CPU  RAM  Disk Autostart    MAC Address        IPv4 Address     IPv6 Address                             Data Disk(s)
----------  ----------  ---  ---  ---- -----------  -----------------  ---------------  ---------------------------------------  ------------
lb          running     2    4G   25   enable       52:54:00:ae:4e:f3  100.73.0.211     2a01:000:000:0000:1715::224              
  \->  17:54:23 up 1 day, 20:53,  0 user,  load average: 0.00, 0.00, 0.00
kc1         running     2    4G   25   enable       52:54:00:8a:0d:80  100.73.0.193     2a01:000:000:0000:1715::14c              
  \->  17:54:23 up 1 day, 20:48,  2 users,  load average: 0.77, 0.47, 0.35
kc2         running     2    4G   25   enable       52:54:00:a4:03:5e  100.73.0.51      2a01:000:000:0000:1715::199              
  \->  17:54:23 up 1 day, 20:46,  0 user,  load average: 0.27, 0.30, 0.25
kc3         running     2    4G   25   enable       52:54:00:ad:e6:f5  100.73.0.235     2a01:000:000:0000:1715::129              
  \->  17:54:23 up 1 day, 20:46,  0 user,  load average: 0.34, 0.30, 0.22
kw1         running     4    16G  40   enable       52:54:00:ec:44:d8  100.73.0.106     2a01:000:000:0000:1715::9d               lhone/20G, longhorn/10G
  \->  17:54:23 up  9:28,  0 user,  load average: 0.05, 0.12, 0.09
kw2         running     4    16G  40   enable       52:54:00:1f:0b:e8  100.73.0.107     2a01:000:000:0000:1715::136              lhone/20G, longhorn/10G
  \->  17:54:23 up 1 day, 20:44,  0 user,  load average: 0.33, 0.16, 0.11
kw3         running     4    16G  40   enable       52:54:00:24:d0:e9  100.73.0.85      2a01:000:000:0000:1715::42               lhone/20G, longhorn/10G
  \->  17:54:23 up 1 day, 50 min,  0 user,  load average: 0.01, 0.06, 0.08
```

## How to use
You need to be root to use these scripts.
Look into variables (at the top of *.sh scripts) to make sure their values fit you.

- dovm.sh create VM
  - password for the specified user can be specified in two ways:
    - parameter '-p' will ask interactively
    - or you can export USER_PASS variable, e.g. `unset USER_PASS; read -p "Enter password: " -s USER_PASS; export USER_PASS; echo`
  - file with SSH public key(s) can be specified within '-u' parameter, default is $HOME/.ssh/authorized_keys
- undovm.sh destroy VM and cleanup environment
- xtachDisk.sh attach/detach data disks to/from VM
- listvms.sh list VMs on this baremetal box

```
Usage: ./dovm.sh vm_name [--user trinity,[ssh_key_file]] [--password] [--cpu 2] [--ram 4] [--disk 25] --intf bridge[,mac]
  '-p' will ask for password interactively OR you can use envvar USER_PASS to pass the password to the script
  file with SSH public key(s) can be specified within '-u' parameter, default is /root/.ssh/authorized_keys
```

```
Usage:
   xtachDisk.sh vm_name (-a [size[,name]] | -d name [-r]) [--unattended|-u]

where:
 '-a [size[,name]]' will attach disk of specified size (default - 10G) and name (default - 'data')
 '-d name' will detach disk, specified by the name
 '-r' will remove data disk, which is subject to detach
```

Other notes:
- VMs are resolvable by their names inside both host and other VMs.
- VM get address dynamically by DHCP.
- You can make static MAC-based assignments using DNSMasq 'dhcp-host' parameter.
- You can pre-generate MAC address using genmac.py tool and pass it with '-i' option to dovm.sh, e.g. `dovm.sh myvm -u myid -c 4 -r 8 -d 50 -i br-ex,d2:c3:7a:cc:a6:0e`

## More baremetal
If you want to add more baremetal boxes - procedure the same for every box.
To make VM names resolvable across all boxes, modify DNSMasq configuration (local addresses, DHCP pools) on every box, reflect additional boxes in 'server' parameter over all baremetals and do not forget to ensure that DNSMasq listens on "external" interfaces ('interface'parameter) in order to process requests from other servers.

Enjoy :)
