#!/bin/bash
#
# Art by doka@funlab.cc

# Where VM disks located
IMG="/var/lib/libvirt/images"

help() {
  echo "Usage:"
  echo "   $0 vm_name (-a [size[,name]] | -d name [-r]) [--unattended|-u]"
  echo
  echo "where:"
  echo " '-a [size[,name]]' will attach disk of specified size (default - 10G) and name (default - 'data')"
  echo " '-d name' will detach disk, specified by the name"
  echo " '-r' will remove data disk, which is subject to detach"
  echo
  exit $1
}

VM_NAME=$1
shift

[ -z "$VM_NAME" ] && help 1

VMs=$(virsh list --all | awk '$1 ~ /[0-9]+/ {print $2}')
if ! echo $VMs | grep -qw $VM_NAME ; then
  echo "VM not defined, stopping ..."
  exit 1
fi

# dd blocksize (bs=) in 'M'
DD_BLOCK_SIZE=4
UNATT=false
REMOVE_DISK=false
while [ -n "$1" ]; do
  case $1 in
    --attach|-a)
      shift
      read DISK_SIZE DISK_ID <<< "$(echo $1 | tr ',' ' ')"
      shift
      if ! [[ $DISK_SIZE =~ ^[0-9]+$ ]]; then
        echo "Disk size must be numerical, in GB"
        exit 1
      fi
      #-DISK_SIZE=$((DISK_SIZE*1024/DD_BLOCK_SIZE))
      DISK_ID=${DISK_ID:-data}
      ;;
    --detach|-d)
      shift
      unset DISK_SIZE
      DISK_ID=$1
      shift
      ;;
    --unattended|-u)
      UNATT=true
      shift
      ;;
    --remove|-r)
      REMOVE_DISK=true
      shift
      ;;
    *)
      help 1
      ;;
  esac
done

if [ $DISK_ID == "attached" ] || [ $DISK_ID == "detached" ] || [[ $DISK_ID =~ - ]]; then
  echo "Sorry for inconvenience, but 'attached', 'detached' and '-' in the disk name are not allowed"
  exit 1
fi

if [ -n "$DISK_SIZE" ]; then
  if ls $IMG/$VM_NAME/$VM_NAME-$DISK_ID-*-attached.raw >/dev/null 2>&1 ; then
    echo "Disk '$DISK_ID' is already attached to the '$VM_NAME'"
    exit 1
  fi

  # Check whether disk exists but not attached
  if DISK_NAME=$(ls $IMG/$VM_NAME/$VM_NAME-$DISK_ID-*-detached.raw 2>/dev/null) ; then
    TARGET=$(basename $DISK_NAME | awk -F[.-] '{print $(NF-2)}')
    # Get last letter
    TARGET=${TARGET:2}
    # Rename disk to attached
    NEW_DISK_NAME=$(echo $DISK_NAME | sed 's/detached/attached/g')
    mv $DISK_NAME $NEW_DISK_NAME
    DISK_NAME=$NEW_DISK_NAME
  else
    # Create _virsh_ mapping vm_name-disk_id-target to be able to detach it later
    for TARGET in {k..z} no_slots ; do
      ls $IMG/$VM_NAME/*-vd${TARGET}-*.raw >/dev/null 2>&1 || break
    done

    # If there are vdk to vdz are already attached, consider it's too much :-)
    if [ $TARGET == "no_slots" ]; then
      echo "Too much disks attached to the '$VM_NAME', detach something else"
      exit 1
    fi

    DISK_NAME=$IMG/$VM_NAME/$VM_NAME-$DISK_ID-vd${TARGET}-attached.raw
    #-dd if=/dev/zero of=$DISK_NAME bs=${DD_BLOCK_SIZE}M count=$DISK_SIZE status=progress
    qemu-img create -f raw $DISK_NAME ${DISK_SIZE}G
  fi

  if virsh attach-disk $VM_NAME --source $DISK_NAME --target vd${TARGET} --persistent ; then
    exit 0
  else
    if [ -n "$NEW_DISK_NAME" ]; then
      # Disk was renamed, not created - rollback
      NEW_DISK_NAME=$(echo $DISK_NAME | sed 's/attached/detached/g')
      mv $DISK_NAME $NEW_DISK_NAME
    else
      rm -vf $DISK_NAME
    fi
    exit 1
  fi

# DISK_ID is for both attach and detach, but 'attach' we already catched above
elif [ -n "$DISK_ID" ]; then
  if DISK_NAME=$(ls $IMG/$VM_NAME/$VM_NAME-$DISK_ID-*-attached.raw 2>/dev/null) ; then
    TARGET=$(basename $DISK_NAME | awk -F[.-] '{print $(NF-2)}')
    if ! $UNATT; then
      read -p "Do you really want to detach '$DISK_ID' from $VM_NAME ? " yn
      [[ $yn =~ ^Y|y ]] || exit 0
    fi

    if virsh detach-disk $VM_NAME $TARGET --persistent ; then
      NEW_DISK_NAME=$(echo $DISK_NAME | sed 's/attached/detached/g')
      mv $DISK_NAME $NEW_DISK_NAME
    fi
  fi

  if $REMOVE_DISK && DISK_NAME=$(ls $IMG/$VM_NAME/$VM_NAME-$DISK_ID-*-detached.raw 2>/dev/null) ; then
    yn="n"
    $UNATT || read -p "Do you really want to physically remove '$DISK_ID' ? " yn
    if [[ $yn =~ ^Y|y ]]; then
      rm -vf $DISK_NAME
    else
      echo
      echo "Disk '$DISK_ID' detached and preserved as $NEW_DISK_NAME"
      echo
    fi
    exit 0
  fi

  if DISK_NAME=$(ls $IMG/$VM_NAME/$VM_NAME-$DISK_ID-*-detached.raw 2>/dev/null) ; then
    echo
    echo "Disk '$DISK_ID' is not attached to the '$VM_NAME'"
    echo
    exit 1
  else
    echo
    echo "Disk '$DISK_ID' for '$VM_NAME' do not exists"
    echo
    exit 1
  fi

else
  help 1
fi
