#!/usr/bin/env python3
#
# Art by doka@funlab.cc

from datetime import datetime
from pytz import utc
import random

def mac_address():
    """
    Generate time-sortable pseudo-random MAC address:
    * 39(+3) bits: milliseconds since Qloude Epoch with:
    - INSERTED control bits (Universal/Local [2nd bit of upper octet], Unicast/Multicast [1st bit])
    - keeping in mind that 0xFE is reserved by libvirt, thus clearing 3th bit
    * 6 random bits
    :return: String repr of MAC address
    """

    total_bits = 48
    time_bits = 39      # 2**39/(365*24*60*60*1000) gives 17.4 years
    start = 2021

    while True:
        ms_since_start = int((datetime.now(tz=utc) - datetime(start, 1, 1, tzinfo=utc)).total_seconds() * 1000)
        since_bits = ms_since_start.bit_length()
        if since_bits <= time_bits:
            break
        else:
            # Wow!!! Another plenty of years passed and we still use this code!!!
            # Lets give it chance for next 12 years
            start += 12

    mac = ms_since_start << (total_bits - since_bits)
    # We need to insert 0b010 (PSB) after first five bits, so saving upper five and shift lower rest bits by two
    # Save 1111.1xxx:...
    saved_upper = mac & 0xF80000000000
    # Save xxxx.x111:...
    saved_lower = (mac & 0x07FFFFFFFFFF) >> 3
    # Joining parts, thus having three bits for U/L, U/M and libvirt reserved
    mac = saved_upper | saved_lower
    # Set U/L bit to be 'locally administered' (41st)
    mac = mac | (1 << 41)
    # Bits 40th and 42nd are already 'zero'
    # 42th bit since 0xFE is reserved by libvirt: mac = mac & ~(1 << 42)
    # 40th (U/M) is unicast: mac = mac & ~(1 << 40)
    # Add lower 6 random bits
    mac = mac | random.getrandbits(6)

    # Strip leading '0x'
    macstr = f'{str(hex(mac))}'[2:]
    return ":".join(macstr[i:i+2] for i in range(0, len(macstr), 2))

if __name__ == "__main__":
    print(mac_address())
    exit(0)

