#!/bin/bash
#
# Art by doka@funlab.cc

OS_TYPE=${OS_TYPE:-"ubuntu24.04"}
OS_NAME=${OS_NAME:-"noble"}
UPDATE_WEEKLY=true
# Where to store VM disks
IMG="/var/lib/libvirt/images"

##### NO CONFIGURABLE PARTS BELOW THIS LINE #####
OS_BASE=${OS_BASE:-"https://cloud-images.ubuntu.com/${OS_NAME}/current/${OS_NAME}-server-cloudimg-amd64.img"}

# Stop on any error
set -e

OS_BASE_NAME=$(echo $OS_BASE | awk -F/ '{print $NF}')
APP_DIR=$(dirname $0)

if ! [ -r "$APP_DIR/user-data-${OS_NAME}.template" ]; then
  echo "$APP_DIR/user-data-${OS_NAME}.template not found, cannot continue ..."
  exit 1
fi

VM_NAME=$1
if [ -z "$VM_NAME" ] || [[ $VM_NAME =~ ^- ]] ; then
  echo
  echo "Usage: $0 vm_name [--user trinity,[ssh_key_file]] [--password] [--cpu 2] [--ram 4] [--disk 25] --intf bridge[,mac]"
  echo "  '-p' will ask for password interactively OR you can use envvar USER_PASS to pass the password to the script"
  echo "  file with SSH public key(s) can be specified within '-u' parameter, default is $HOME/.ssh/authorized_keys"
  echo
  exit 1
fi
shift

while [ -n "$1" ]; do
  case $1 in
    -h|--help)
      exec $0
      ;;
    -u|--user)
      shift; USER_INFO=$1
      read USER_NAME USER_KEY <<< "$(echo $USER_INFO | tr ',' ' ')"
      ;;
    -p|--password)
      ENTPASS=""
      while [ -z "$ENTPASS" ]; do
        read -rs -p "Enter password: " ENTPASS
        echo
      done
      ENTPASS_VRFY=$RANDOM
      while [ "$ENTPASS" != "$ENTPASS_VRFY" ]; do
        read -rs -p "Verify password: " ENTPASS_VRFY
        echo
      done
      export USER_PASS=$ENTPASS
      ;;
    -c|--cpu)
      shift; CPU=$1 ;;
    -r|--ram)
      shift; RAM=$1 ;;
    -d|--disk)
      shift; DISK=$1 ;;
    -i|--intf)
      shift; INTF=$1 ;;
  esac
  shift
done

if [ -z "$INTF" ]; then
  echo "At least one interface need to be specified"
  exec $0
fi

[ -z "$USER_KEY" ] && USER_KEY=$HOME/.ssh/authorized_keys
if ! [ -r "$USER_KEY" ]; then
  echo "$USER_KEY is unreadable, stopping ..."
  exit 1
fi

[ -z "$USER_NAME" ] && USER_NAME='trinity'
[ -z "$USER_PASS" ] && USER_PASS=$(cat /dev/urandom | tr -dc 'a-zA-Z0-9' | fold -w 32 | head -n 1)
USER_PASS=$(echo -n $USER_PASS | mkpasswd --method=SHA-512 --rounds=4096 --stdin | tail -1)
[ -z $CPU ] && CPU=2
[ -z $RAM ] && RAM=4
[ -z "$DISK" ] && DISK=25

# Download cloud image
dpkg -s curl >/dev/null 2>&1 || apt -y install curl
mkdir -p $IMG/base

# Exit if curl will fail
set -e
[ -f "$IMG/base/$OS_BASE_NAME" ] || curl -o $IMG/base/$OS_BASE_NAME $OS_BASE
if $UPDATE_WEEKLY ; then
  now=$(date +%s)
  modified=$(stat -c %Y $IMG/base/$OS_BASE_NAME)
  # TODO checksum compare. "I will kiss you... sometime... may be..."
  [ $((now-modified)) -gt $((7*24*60*60)) ] && curl -o $IMG/base/$OS_BASE_NAME $OS_BASE
fi
set +e

VMs=$(virsh list --all | awk '$1 ~ /[0-9]+/ {print $2}')
if echo $VMs | grep -qw $VM_NAME ; then
  echo "VM defined, undefine it before"
  exit 1
fi

mkdir -p $IMG/$VM_NAME

# Add all keys into a single line, wrapped with double-quotes and remove trailing comma(s)
USER_KEYS=$(cat $USER_KEY | egrep -v "^\s*$" | sed -E 's/^/"/g; s/$/"/g;' | tr '\n' ',' | sed -E 's/,+$//g')

# Prepare network connectivity
read IN MAC <<< "$(echo $INTF | tr ',' ' ')"
[ -n "$MAC" ] && MAC=",mac=$MAC"
CONN_STR="--network bridge=$IN,model=virtio,virtualport_type=openvswitch${MAC}"

for f in meta-data user-data-${OS_NAME} ; do
  cat $APP_DIR/$f.template | sed "
    s|%VM_NAME%|$VM_NAME|g;
    s|%USER_NAME%|$USER_NAME|g;
    s|%USER_PASS%|$USER_PASS|g;
    s|%USER_KEYS%|$USER_KEYS|g;
    " > /var/lib/libvirt/images/$VM_NAME/$f
done
mv -v $IMG/$VM_NAME/user-data-${OS_NAME} $IMG/$VM_NAME/user-data
genisoimage -output $IMG/$VM_NAME/$VM_NAME-cidata.iso -volid cidata -joliet -rock \
	$IMG/$VM_NAME/user-data \
	$IMG/$VM_NAME/meta-data

qemu-img create -f qcow2 -F qcow2 -o backing_file=$IMG/base/$OS_BASE_NAME $IMG/$VM_NAME/$VM_NAME.qcow2
qemu-img convert -p -W -f qcow2 -O raw $IMG/$VM_NAME/$VM_NAME.qcow2 $IMG/$VM_NAME/$VM_NAME.img
qemu-img resize -f raw --preallocation=falloc $IMG/$VM_NAME/$VM_NAME.img ${DISK}G
rm -vf $IMG/$VM_NAME/$VM_NAME.qcow2

virt-install --connect qemu:///system --virt-type kvm --name $VM_NAME --ram $((RAM*1024)) --vcpus=$CPU --os-variant $OS_TYPE --import \
	--disk path=$IMG/$VM_NAME/$VM_NAME.img,bus=virtio \
	--disk $IMG/$VM_NAME/$VM_NAME-cidata.iso,device=cdrom \
	--boot hd \
  $CONN_STR \
	--console pty,target_type=serial --graphics none --noautoconsole

virsh autostart $VM_NAME

echo
echo "You can connect to console: virsh console $VM_NAME"
echo "Or connect over SSH: ssh $USER_NAME@$VM_NAME"
echo
