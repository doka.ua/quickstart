#!/bin/bash
#
# Art by doka@funlab.cc

[ "$(systemd-detect-virt)" == "none" ] && IS_VIRT="generic" || IS_VIRT="virtual"

. /etc/os-release
case $UBUNTU_CODENAME in
  focal|jammy)
    KERNEL="linux-image-${IS_VIRT}-hwe-${VERSION_ID}"
    SSH_SOCK=false
    ;;
  lunar)
    # No HWE images for short-living releases
    SSH_SOCK=true
    ;;
  noble)
    KERNEL="linux-image-${IS_VIRT}-hwe-${VERSION_ID}"
    SSH_SOCK=true
    ;;
  *)
    echo Unsupported operation system, exiting...
	  exit 1
esac

SSH_PORT=${SSH_PORT:-22}
[ "$BOOT_UPG" != "true" ] && BOOT_UPG=false
# Construct FQDN
WAN_HOST=${WAN_HOST:-$(hostname --fqdn)}
if ! [[ $WAN_HOST =~ \. ]]; then
  DOMAIN_NAME=$(cat /etc/resolv.conf | grep -i ^search | awk '{print $2}')
  [[ $DOMAIN_NAME =~ ^[A-Za-z0-9\-.]$ ]] &&
    WAN_HOST="$(echo "$WAN_HOST" | sed -E 's/^\.+//g').$DOMAIN_NAME)"
fi

while [ -n "$1" ]; do
  case $1 in
    -p)
      shift; SSH_PORT=$1; shift
      ;;
    -b)
      BOOT_UPG=true
      shift
      ;;
    -d)
      shift; WAN_HOST=$1; shift
      ;;
    -h)
      echo "$0 [-p SSH_PORT] [-b] [-d FQDN]"
      echo "where '-b' instructs unattended upgrades to reboot VM if it will be required"
      exit 0
      ;;
  esac
done

if [[ $SSH_PORT =~ ^[0-9]+$ ]] && [ $SSH_PORT -gt 0 ] && [ $SSH_PORT -le 65535 ]; then
  true
else
  echo "SSH_PORT must be in range 1-65535"
  exit 1
fi

echo "CONFIGURED:"
echo "* SSH PORT: $SSH_PORT"
echo "* BOOT ON UPGRADE: $BOOT_UPG"
echo "* HOSTNAME: $WAN_HOST"
echo "* KERNEL: $KERNEL"
echo
echo "You have 5 seconds to press Ctrl-C :-)"
sleep 5

## Add proposed repository
if ! egrep -qri "^deb.*proposed" /etc/apt/ ; then
	proposed=$(lsb_release -s -c)-proposed
	echo Registering ${proposed}
	echo "deb http://archive.ubuntu.com/ubuntu/ ${proposed} main restricted universe multiverse" > /etc/apt/sources.list.d/proposed.list
fi
apt-get update

INIT_PKGS="net-tools jq curl tmux unattended-upgrades ipset iptables-persistent netfilter-persistent $KERNEL"
DEBIAN_FRONTEND=noninteractive apt-get -y install $INIT_PKGS
DEBIAN_FRONTEND=noninteractive apt-get -y full-upgrade

HOST_NAME=$(echo $WAN_HOST |cut -d. -f1)
echo $HOST_NAME > /etc/hostname
######################
## WAN_NAME=$(ip -j route |jq .[] |jq 'select (.dst == "default")' |jq -s 'min_by(.metric)' |jq -r .dev)
## WAN4_IP=$(ip -j a |jq .[] |jq "select (.ifname == \"$WAN_NAME\") .addr_info" |jq .[] |
##             jq -r 'select (.family == "inet") .local')
## WAN6_IP=$(ip -j a |jq .[] |jq "select (.ifname == \"$WAN_NAME\") .addr_info" |jq .[] |
##             jq -r 'select (.family == "inet6") .local' | grep -v fe80)
######################
# Resolve of WAN IPs is the DNS's job, here are just loopbacks to define hostname/domain 
#sed -Ei "
#        s/^127\.0.([0-9]+\.[0-9]+).*$/127.0.\1   $WAN_HOST $HOST_NAME localhost/;
#        s/^::1\s*.*$/::1   $WAN_HOST $HOST_NAME ip6-localhost ip6-loopback/
#       " /etc/hosts
sed -Ei "
        s/^127\.0.([0-9]+\.[0-9]+).*$/127.0.\1   localhost/;
        s/^::1\s*.*$/::1   ip6-localhost ip6-loopback/
       " /etc/hosts

# Change SSH port
if $SSH_SOCK ; then
  # TODO move to override
  sed -Ei "s/^[# \t]*ListenStream\s*=.*$/ListenStream=${SSH_PORT}/gi" /usr/lib/systemd/system/ssh.socket
  systemctl daemon-reload
  systemctl restart ssh.socket
else
  sed -Ei "s/^[# \t]*port\s+.*$/Port ${SSH_PORT}/gi" /etc/ssh/sshd_config
  systemctl restart ssh
fi

## --- ipset service
#cat <<EOF >/usr/lib/systemd/system/ipset-persistent.service
#[Unit]
#Description=ipset persistancy service
#DefaultDependencies=no
#Requires=netfilter-persistent.service
##Requires=ufw.service
#Before=network.target
#Before=netfilter-persistent.service
##Before=ufw.service
#ConditionFileNotEmpty=/etc/ipsets.conf
#
#[Service]
#Type=oneshot
#RemainAfterExit=yes
#ExecStart=/sbin/ipset restore -! -f /etc/ipsets.conf
## save on service stop, system shutdown etc.
##ExecStop=/sbin/ipset save blacklist -f /tmp/blacklist.conf
#
#[Install]
#WantedBy=multi-user.target
##RequiredBy=ufw.service
#RequiredBy=netfilter-persistent.service
#EOF
#
#cat <<EOF >/etc/ipsets.conf
## --- RFC1918 networks (https://www.iana.org/assignments/iana-ipv4-special-registry/iana-ipv4-special-registry.xhtml)
##create rfc1918 nethash
##flush rfc1918
##add rfc1918 10.0.0.0/8
##add rfc1918 172.16.0.0/12
##add rfc1918 192.168.0.0/16
#EOF
#
#systemctl enable ipset-persistent
systemctl enable netfilter-persistent

# --- Unattended upgrades
APTCD=/etc/apt/apt.conf.d
RULES=95auto-upgrades-lito
OMOD=${APTCD}/50unattended-upgrades
NMOD=${APTCD}/95unattended-upgrades-lito

cp -v ${OMOD} ${NMOD}
# s|^(\s*(//)*\s*)*(Unattended-Upgrade::Mail).*$|\3 "root";|g;
sed -Ei '
  s|^(\s*(//)*\s*)*(Unattended-Upgrade::Remove-Unused-Kernel-Packages).*$|\3 "true";|g;
  s|^(\s*(//)*\s*)*(Unattended-Upgrade::Remove-Unused-Dependencies).*$|\3 "true";|g;
  s|^(\s*(//)*\s*)*(Unattended-Upgrade::SyslogEnable).*$|\3 "true";|g;
  s|^(\s*(//)*\s*)*(Unattended-Upgrade::MinimalSteps).*$|\3 "true";|g;
  s|^(\s*(//)*\s*)*(.*dis.*-security";)$|\t\3|g;
  s|^(\s*(//)*\s*)*(.*dis.*-updates";)$|\t\3|g;
  s|^(\s*(//)*\s*)*(.*dis.*-proposed";)$|\t\3|g;
  s|^(\s*(//)*\s*)*(.*dis.*-backports";)$|\t\3|g;
  ' ${NMOD}

$BOOT_UPG && \
  sed -Ei '
    s|^(\s*(//)*\s*)*(Unattended-Upgrade::Automatic-Reboot ).*$|\3 "true";|g;
    ' ${NMOD}

cat <<EOF >$APTCD/$RULES
APT::Periodic::Update-Package-Lists "1";
APT::Periodic::Unattended-Upgrade "1";
APT::Periodic::Download-Upgradeable-Packages "1";
APT::Periodic::AutocleanInterval "7";
# Preserve modified configs
Dpkg::Options {
   "--force-confdef";
   "--force-confold";
};
EOF

## --- Rsyslog MaxMessageSize and passthrough escape sequences (e.g. \n)
#cat <<EOF >/etc/rsyslog.d/90-msgsizefmt.conf
#\$MaxMessageSize 128k
#\$EscapeControlCharactersOnReceive off
#EOF
#systemctl restart rsyslog

#read -p "Disable RP filter? (yes/no) (default is no) " q2
#[ "$q2" = "yes" ] && VAL=0 || VAL=1
#sed -Ei "
#  s/^.*default\.rp_filter.*$/net.ipv4.conf.default.rp_filter=${VAL}/g;
#  s/^.*all\.rp_filter.*$/net.ipv4.conf.all.rp_filter=${VAL}/g;
#  " /etc/sysctl.conf
#sysctl -p

# --- timesyncd
sed -Ei '
  s/^(#|\s)*NTP\s*=.*$/NTP=0.de.pool.ntp.org 1.de.pool.ntp.org 0.nl.pool.ntp.org 1.nl.pool.ntp.org/g
  ' /etc/systemd/timesyncd.conf
systemctl restart systemd-timesyncd
timedatectl set-ntp true
timedatectl set-timezone UTC

DEBIAN_FRONTEND=noninteractive apt-get -y autoremove --purge

# --- finalize
[ -f /var/run/reboot-required ] && shutdown -r now

